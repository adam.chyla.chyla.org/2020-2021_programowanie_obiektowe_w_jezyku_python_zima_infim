# Samochod
#         -> SamochodLpg (+ dodamy kilka metod)
#         -> SamochodElek (+ dodamy kilka metod)

class Samochod:
	def __init__(self, kolor, marka):
		self.kolor = kolor
		self.marka = marka

	# tempate method
	def run(self):
		if self.czy_odpowiednia_ilosc_paliwa():
			print("Wszystko gotowe, jedziemy...",
				  self.kolor, 
				  self.marka)
			self.zmniejsz_stan_paliwa()
		else:
			print("Za malo paliwa... zatankuj!")


class SamochodElek(Samochod):
	def __init__(self, kolor, marka):
		super().__init__(kolor, marka)
		self.akumulator = 0 

	def zatankuj(self, ile):
		self.akumulator += ile

	def czy_odpowiednia_ilosc_paliwa(self):
		return self.akumulator >= 15

	def zmniejsz_stan_paliwa(self):
		self.akumulator -= 15

class SamochodLpg(Samochod):
	def __init__(self, kolor, marka):
		super().__init__(kolor, marka)
		self.butla = 0
		
	def zatankuj(self, ile):
		self.butla += ile
	
	def czy_odpowiednia_ilosc_paliwa(self):
		return self.butla > 10
	
	def zmniejsz_stan_paliwa(self):
		self.butla -= 10
		
s1 = SamochodElek('czewony', 'fiat')
s1.zatankuj(30)
s1.run()

s2 = SamochodLpg('czarny', 'bmw')
s2.zatankuj(50)
s2.run()
