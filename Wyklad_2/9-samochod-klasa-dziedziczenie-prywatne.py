# Samochod
#         -> SamochodLpg (+ dodamy kilka metod)
#         -> SamochodElek (+ dodamy kilka metod)

class Samochod:
	def __init__(self, kolor, marka, nr_rej):
		self.kolor = kolor
		self.marka = marka
		self._nr_rej = nr_rej

	# tempate method
	def run(self):
		if self.czy_odpowiednia_ilosc_paliwa():
			print("Wszystko gotowe, jedziemy...",
				  self.kolor, 
				  self.marka)
			self.zmniejsz_stan_paliwa()
		else:
			print("Za malo paliwa... zatankuj!")


class SamochodElek(Samochod):
	def __init__(self, kolor, marka, nr_rej):
		super().__init__(kolor, marka, nr_rej)
		self.akumulator = 0 

	def zatankuj(self, ile):
		self.akumulator += ile

	def czy_odpowiednia_ilosc_paliwa(self):
		return self.akumulator >= 15

	def zmniejsz_stan_paliwa(self):
		self.akumulator -= 15

class SamochodLpg(Samochod):
	def __init__(self, kolor, marka, nr_rej):
		super().__init__(kolor, marka, nr_rej)
		self.butla = 0
		
	def zatankuj(self, ile):
		print("Zatankowano:", self._nr_rej)
		self.butla += ile
	
	def czy_odpowiednia_ilosc_paliwa(self):
		return self.butla > 10
	
	def zmniejsz_stan_paliwa(self):
		self.butla -= 10
		
s1 = SamochodElek('czewony', 'fiat', '123')
s1.zatankuj(30)
s1.run()

s2 = SamochodLpg('czarny', 'bmw', '123')
s2.zatankuj(50)
s2.run()
