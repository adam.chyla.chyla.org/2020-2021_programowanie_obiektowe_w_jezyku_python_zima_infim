class Samochod: # PEP8 - uzycie self
	def __init__(self, kolor, marka): # metoda
		self.kolor = kolor
		self.marka = marka
		self._paliwo = 0 # PEP8 - podloga na poczatku nazwy oznacza pole prywatne
	
	def zatankuj(self, ile):
		self._paliwo += ile
	
	def run(self):
		if self._paliwo < 15:
			print("Za malo paliwa... zatankuj!")
		else:
			print("Wszystko gotowe, jedziemy...",
				  self.kolor, 
				  self.marka,
				  self._paliwo)
			self._paliwo -= 15

s1 = Samochod('czerwony', 'fiat')
s2 = Samochod('czarny', 'bmw')

s1.zatankuj(30)

s1._paliwo = 100

s1.run()
s2.run()

s1.run()

s1.run()