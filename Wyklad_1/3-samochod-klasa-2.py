class Samochod: # PEP8 - uzycie self
	def init(self, kolor, marka): # metoda
		self.kolor = kolor
		self.marka = marka
	

s1 = Samochod()
s1.init('czerwony', 'fiat')

s2 = Samochod()
s2.init('czarny', 'bmw')

def run(sam):
	print("Wszystko gotowe, jedziemy...", sam.kolor, sam.marka)


run(s1)
run(s2)
	